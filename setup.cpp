#include <app.h>

ButtonDriver *_Button;
AQM0802ADriver *_LCD;
LEDDriver *_LED;

void setup() {

    // Initialize Button
    _Button = new ButtonDriver(Button_PIN);
    Button.setup();
    // Initialize LCD
    _LCD = new AQM0802ADriver(I2C);
    LCD.connect();
    _LED = new LEDDriver(LED_PIN);
}
