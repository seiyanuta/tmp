#ifndef __MAKESTACK_SETUP_H__
#define __MAKESTACK_SETUP_H__

extern ButtonDriver *_Button;
#define Button (*_Button)
extern AQM0802ADriver *_LCD;
#define LCD (*_LCD)
extern LEDDriver *_LED;
#define LED (*_LED)

#endif
