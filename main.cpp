#include <app.h>

void app() {

    Button.on_pressed([]() {
      LCD.update("plz wait");
      Endpoint.invoke("button", "pressed!");
    });

    Store.watch("message", [](String new_msg) {
      LCD.update(new_msg);
    });
}

