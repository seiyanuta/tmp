
#ifndef __MAKESTACK_APP_H__
#define __MAKESTACK_APP_H__

/* `_app.h' will be generated in build system. It defines fundamental types such as
   uint8_t and MakeStack APIs. */
#include <_app.h>

// Button pin
#define Button_PIN 14
#define LED_PIN 12

#endif
